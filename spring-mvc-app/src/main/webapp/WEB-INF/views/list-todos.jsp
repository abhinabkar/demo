<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Todos</title>
	<link href="webjars/bootstrap/3.3.6/css/bootstrap.min.css"
		    		rel="stylesheet">
</head>
<body>

	<%@ include file="common/navigation.jspf"%>
	
	<div class="container">
		Hello ${name} <br>
		<br>
		
		<table class="table table-striped">
			<caption>Your Todos Are,</caption>
			<thead>
				<tr>
					<th>Id</th>
					<th>User</th>
					<th>Desc</th>
					<th>Target Date</th>
					<th>Is Completed?</th>
				</tr>
			</thead>
				<c:forEach items="${todos}" var="todo">
					<tr>
						<td>${todo.id}</td>
						<td>${todo.user} </td>
						<td>${todo.desc}</td>
						<td><fmt:formatDate pattern="dd/MM/yyyy"
								value="${todo.targetDate}" /></td>
						<td>${todo.done}</td>
						<td><a href="/update-todo?id=${todo.id}" class="btn btn-success">Update</a></td>
						<td><a href="/delete-todo?id=${todo.id}" class="btn btn-danger">Delete</a></td>
					</tr>
				</c:forEach>
			<tbody>
				
			</tbody>
		</table>
		
		<div>
			<a class="btn btn-success" href="/add-todo">Add a Todo</a>
		</div>
	</div>
	
	<br>
	
	<script src="webjars/jquery/2.1.4/jquery.min.js"></script>
	<script src="webjars/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</body>
</html>