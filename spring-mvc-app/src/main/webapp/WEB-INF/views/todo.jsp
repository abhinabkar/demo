<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Todo</title>
	<link href="webjars/bootstrap/3.3.6/css/bootstrap.min.css"
		    		rel="stylesheet">
</head>
<body>

	<%@ include file="common/navigation.jspf"%>
	<div class="container">
	
		<h1>Add a Todo</h1> <br>
		<form:form method="post" commandName="todo">
		
			<form:hidden path="id"/>
			<form:hidden path="user"/>
			<fieldset class="form-group">
				<form:label path="desc">Description</form:label> 
				<form:input name="desc" path="desc" type="text" required="required" class="form-control"/>
				<form:errors path="desc" cssClass="text-warning" />
			</fieldset>
			
			<fieldset class="form-group">
				<form:label path="targetDate">Target Date</form:label> 
				<form:input path="targetDate" type="text" required="required" class="form-control"/>
				<form:errors path="targetDate" cssClass="text-warning" />
			</fieldset>
			<input type="submit" value="Submit" class="btn btn-success"/>
		</form:form>
	</div>
	
	<script src="webjars/jquery/2.1.4/jquery.min.js"></script>
	<script src="webjars/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</body>
</html>