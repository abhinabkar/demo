package com.abhinab.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import com.abhinab.bean.Todo;

@Repository
public class JdbcDaoImpl {
	
	public Todo getTodo(Integer id) {
		
		Connection connection = null;
		
		try {
			String driver = "oracle.jdbc.driver.OracleDriver";
			Class.forName(driver).newInstance();
			connection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","system","system123");
			PreparedStatement ps = connection.prepareStatement("select * from AK_DB.Todo where ID= ?");
			ps.setInt(1, id);
				
			Todo todo = null;
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()) {
				todo = new Todo();
				todo.setId(rs.getInt("id"));
				todo.setUser(rs.getString("username"));
				todo.setDesc(rs.getString("description"));
				todo.setTargetDate(rs.getDate("targetdate"));
				todo.setIsDone(rs.getInt("isdone"));
			}
			
			rs.close();
			ps.close();
			return todo;
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(connection!=null)
					connection.close();
			} catch(SQLException e) {
				
			}
		}
		return null;
	}
	
	public static void main(String args[]) {
		Todo todo = new JdbcDaoImpl().getTodo(1);
		if(todo!=null)
			System.out.println(todo.getDesc()+" before "+todo.getTargetDate());
	}
}
