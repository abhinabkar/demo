package com.abhinab.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.abhinab.bean.HeedspaceEmailTemplate;
import com.abhinab.bean.HeedspaceBlog;
import com.abhinab.bean.HeedspaceConfig;
import com.abhinab.bean.HeedspaceEmailHistory;
import com.abhinab.bean.HeedspaceUser;
import com.abhinab.dao.base.HeedspaceDao;
import com.abhinab.rowmapper.HeedspaceBlogRowMapper;
import com.abhinab.rowmapper.HeedspaceConfigRowMapper;
import com.abhinab.rowmapper.HeedspaceEmailHistoryRowMapper;
import com.abhinab.rowmapper.HeedspaceEmailTemplateRowMapper;
import com.abhinab.rowmapper.HeedspaceUserRowMapper;

@Repository
public class HeedspaceDaoImpl implements HeedspaceDao {

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public HeedspaceDaoImpl(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public int[] insertHeedspaceUsers(List<HeedspaceUser> heedspaceUsers) {
		String sql = "insert into heedspace_user(first_name, last_name, email) values( ?, ?, ? ) ";
		List<Object[]> batchArgs = new ArrayList<>();
		for (HeedspaceUser heedspaceUser : heedspaceUsers) {
			Object[] args = new Object[] { heedspaceUser.getFirstName(), heedspaceUser.getLastName(),
					heedspaceUser.getEmail() };
			batchArgs.add(args);
		}

		return jdbcTemplate.batchUpdate(sql, batchArgs);
	}

	@Override
	public int[] insertHeedspaceBlogs(List<HeedspaceBlog> heedspaceBlogs) {
		String sql = "insert into heedspace_blog(blog_category, blog_name, blog_url) values( ?, ?, ? ) ";
		List<Object[]> batchArgs = new ArrayList<>();
		for (HeedspaceBlog heedspaceBlog : heedspaceBlogs) {
			Object[] args = new Object[] { heedspaceBlog.getBlogCategory(), heedspaceBlog.getBlogName(),
					heedspaceBlog.getBlogUrl() };
			batchArgs.add(args);
		}
		return jdbcTemplate.batchUpdate(sql, batchArgs);
	}

	@Override
	public int[] insertHeedspaceEmailHistories(List<HeedspaceEmailHistory> heedspaceEmailHistories) {
		String sql = "insert into heedspace_email_history(user_id, blog_id, email_sent) values( ?, ?, ? ) ";
		List<Object[]> batchArgs = new ArrayList<>();
		for (HeedspaceEmailHistory heedspaceEmailHistory : heedspaceEmailHistories) {
			Object[] args = new Object[] { heedspaceEmailHistory.getUserId(), heedspaceEmailHistory.getBlogId(),
					heedspaceEmailHistory.getEmailSent() };
			batchArgs.add(args);
		}
		return jdbcTemplate.batchUpdate(sql, batchArgs);
	}

	@Override
	public List<HeedspaceUser> findHeedspaceUsers() {
		String sql = "select user_id, first_name, last_name, email, modified_on from heedspace_user ";
		return jdbcTemplate.query(sql, new HeedspaceUserRowMapper());
	}

	@Override
	public List<HeedspaceUser> findHeedspaceUserByUserId(Long userId) {
		String sql = "select user_id, first_name, last_name, email, modified_on from heedspace_user where user_id = ? ";
		return jdbcTemplate.query(sql, new Object[] { userId }, new HeedspaceUserRowMapper());
	}

	@Override
	public List<HeedspaceUser> findHeedspaceUserByUserEmail(String email) {
		String sql = "select user_id, first_name, last_name, email, modified_on from heedspace_user where email = ? ";
		return jdbcTemplate.query(sql, new Object[] { email }, new HeedspaceUserRowMapper());
	}

	@Override
	public List<HeedspaceBlog> findHeedspaceBlogs() {
		String sql = "select blog_id, blog_name, blog_category, blog_url, modified_on from heedspace_blog ";
		return jdbcTemplate.query(sql, new HeedspaceBlogRowMapper());
	}

	@Override
	public List<HeedspaceBlog> findHeedspaceBlogByBlogId(Long blogId) {
		String sql = "select blog_id, blog_name, blog_category, blog_url, modified_on from heedspace_blog where blog_id = ? ";
		return jdbcTemplate.query(sql, new Object[] { blogId }, new HeedspaceBlogRowMapper());
	}

	@Override
	public List<HeedspaceEmailHistory> findHeedspaceEmailHistories() {
		String sql = "select heh.history_id, heh.user_id, heh.blog_id, heh.email_sent, heh.modified_on "
				+ " hu.first_name, hu.last_name, hu.email, hb.blog_name, hb.blog_category, hb.blog_url "
				+ " from heedspace_email_history heh " + " inner join heedspace_user hu on hu.user_id = heh.user_id "
				+ " inner join heedspace_blog hb on hb.blog_id = heh.blog_id ";
		return jdbcTemplate.query(sql, new HeedspaceEmailHistoryRowMapper());
	}

	@Override
	public List<HeedspaceEmailHistory> findHeedspaceEmailHistoryByUserId(Long userId) {
		String sql = "select heh.history_id, heh.user_id, heh.blog_id, heh.email_sent, heh.modified_on "
				+ " hu.first_name, hu.last_name, hu.email, hb.blog_name, hb.blog_category, hb.blog_url "
				+ " from heedspace_email_history heh " + " inner join heedspace_user hu on hu.user_id = heh.user_id "
				+ " inner join heedspace_blog hb on hb.blog_id = heh.blog_id " + " where heh.user_id = ? ";
		return jdbcTemplate.query(sql, new Object[] { userId }, new HeedspaceEmailHistoryRowMapper());
	}
	
	@Override
	public List<HeedspaceEmailTemplate> findHeedspaceEmailTemplates() {
		String sql = "select template_id, template_name, template, modified_on from heedspace_email_templates";
		return jdbcTemplate.query(sql, new HeedspaceEmailTemplateRowMapper());
	}
	
	@Override
	public List<HeedspaceConfig> findHeedspaceConfigs() {
		String sql = "select config_id, config_name, config_value from heedspace_configs";
		return jdbcTemplate.query(sql, new HeedspaceConfigRowMapper());
	}
	
	@Override
	public Long findMaxIdForHeedspaceBlogs() {
		String sql = "select max(blog_id) from heedspace_blog";
		return jdbcTemplate.queryForObject(sql, Long.class);
	}
	
	@Override
	public int updateHeedspaceConfigs(String configName, String configValue) {
		String sql = "update heedspace_configs set config_value = ? where config_name = ? ";
		return jdbcTemplate.update(sql, configValue, configName);
	}
}
