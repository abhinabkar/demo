package com.abhinab.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Repository;
import com.abhinab.bean.Todo;
import com.abhinab.beanentity.TodoEntity;

@Repository
public class TodoRepository {
	
	public static void main(String args[])
	{
		TodoRepository todoRepository = new TodoRepository();
		Todo todo = new Todo();
		
		todo.setDesc("To complete Jdbc template");
		todo.setIsDone(0);
		todo.setId(2);
		todo.setTargetDate(null);
		todo.setUser("Abhinab");
		
		todoRepository.addTodos(todo);
	}
	
	public void addTodos(Todo todo)
	{
		
		SessionFactory factory=null; 
		Session session=null;
		try
		{
			Configuration config=new Configuration();
			config.configure("hibernate.cfg.xml");
			
			//creating session factory object    
			factory=config.buildSessionFactory(); 
			
			//creating session object    
			session=factory.openSession();   
			
			
			//creating transaction object    
			Transaction t=session.beginTransaction();    
			        
			TodoEntity todoEntity=new TodoEntity();
			todoEntity.setDesc(todo.getDesc());
			todoEntity.setIsDone(todo.getIsDone());
			todoEntity.setId(todo.getId());
			todoEntity.setTargetDate(todo.getTargetDate());
			todoEntity.setUser(todo.getUser());
			
			session.persist(todoEntity);//persisting the object    
			    
			t.commit();//transaction is commited    
		}
		catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
		finally
		{
			if(session!=null)
				session.close();
			
			if(factory!=null)
				factory.close();
		}
	}

}
