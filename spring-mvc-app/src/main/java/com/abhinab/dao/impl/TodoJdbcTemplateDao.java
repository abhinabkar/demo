package com.abhinab.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import com.abhinab.bean.Todo;

@Repository
public class TodoJdbcTemplateDao {

	@Autowired
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}
	
	
	// Select Statements
	
	public Integer getTodosCount(int todoId) {
		String sql = "select count(*) from AK_DB.Todo where id = ? ";
		Integer count = jdbcTemplate.queryForObject(sql, new Object[] {todoId}, Integer.class);
		
		return count;
	}
	
	public Todo getTodoById(int todoId) {
		String sql = "select * from AK_DB.Todo where id = ?";
		return jdbcTemplate.queryForObject(sql, new Object[] {todoId}, new TodoMapper());
	}
	
	public Todo getTodoByIdAndUserUsingNamedJdbcTemplate(int todoId, String username) {
		String sql = "select * from AK_DB.Todo where id = :id and username = :user";
		SqlParameterSource namedParameters = new MapSqlParameterSource("id", todoId)
												.addValue("user", username);
		return namedParameterJdbcTemplate.queryForObject(sql, namedParameters, new TodoMapper());
	}
	
	public List<Todo> getAllTodos() {
		String sql = "select * from AK_DB.Todo";
		return jdbcTemplate.query(sql, new TodoMapper());
	}
	
	
	// Insert Statements
	
	public void insertTodo(Todo todo) {
		String sql = "insert into AK_DB.TODO values(?, ?, ?, ?, ?)";
		jdbcTemplate.update(sql, new Object[] { todo.getId(), todo.getUser(), todo.getDesc(),
				todo.getTargetDate(), todo.getIsDone() } );
	}
	
	
	
	// DDL Statements, but not a good practice
	
	public void createTable() {
		String sql = "create table AK_DB.TEMP (ID INTEGER)";
		jdbcTemplate.execute(sql);
	}
	
	
	// Old Way
	public Todo getTodo(Integer id) {
		
		Connection connection = null;
		
		try {
			connection = dataSource.getConnection();
			PreparedStatement ps = connection.prepareStatement("select * from AK_DB.Todo where ID= ?");
			ps.setInt(1, id);
				
			Todo todo = null;
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()) {
				todo = new Todo();
				todo.setId(rs.getInt("id"));
				todo.setUser(rs.getString("username"));
				todo.setDesc(rs.getString("description"));
				todo.setTargetDate(rs.getDate("targetdate"));
				todo.setIsDone(rs.getInt("isdone"));
			}
			
			rs.close();
			ps.close();
			return todo;
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(connection!=null)
					connection.close();
			} catch(SQLException e) {
				
			}
		}
		return null;

	}
}
