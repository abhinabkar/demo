package com.abhinab.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.abhinab.bean.Todo;

public final class TodoMapper implements RowMapper<Todo> {

	@Override
	public Todo mapRow(ResultSet resultSet, int rowNums) throws SQLException {
		// TODO Auto-generated method stub
		Todo todo = new Todo();
		todo.setId(resultSet.getInt("id"));
		todo.setUser(resultSet.getString("username"));
		todo.setDesc(resultSet.getString("description"));
		todo.setTargetDate(resultSet.getDate("targetdate"));
		todo.setIsDone(resultSet.getInt("isdone"));
		return todo;
	}

}
