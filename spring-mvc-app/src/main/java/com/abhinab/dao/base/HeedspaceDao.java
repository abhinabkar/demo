package com.abhinab.dao.base;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.abhinab.bean.HeedspaceBlog;
import com.abhinab.bean.HeedspaceConfig;
import com.abhinab.bean.HeedspaceEmailHistory;
import com.abhinab.bean.HeedspaceEmailTemplate;
import com.abhinab.bean.HeedspaceUser;

@Repository
public interface HeedspaceDao {

	int[] insertHeedspaceUsers(List<HeedspaceUser> heedspaceUsers);

	int[] insertHeedspaceBlogs(List<HeedspaceBlog> heedspaceBlogs);

	int[] insertHeedspaceEmailHistories(List<HeedspaceEmailHistory> heedspaceEmailHistories);

	List<HeedspaceUser> findHeedspaceUsers();

	List<HeedspaceUser> findHeedspaceUserByUserId(Long userId);

	List<HeedspaceUser> findHeedspaceUserByUserEmail(String email);

	List<HeedspaceBlog> findHeedspaceBlogs();

	List<HeedspaceBlog> findHeedspaceBlogByBlogId(Long blogId);

	List<HeedspaceEmailHistory> findHeedspaceEmailHistories();

	List<HeedspaceEmailHistory> findHeedspaceEmailHistoryByUserId(Long userId);

	List<HeedspaceEmailTemplate> findHeedspaceEmailTemplates();

	List<HeedspaceConfig> findHeedspaceConfigs();

	Long findMaxIdForHeedspaceBlogs();

	int updateHeedspaceConfigs(String configName, String configValue);
	
}
