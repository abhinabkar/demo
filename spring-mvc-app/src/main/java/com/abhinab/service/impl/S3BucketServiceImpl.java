package com.abhinab.service.impl;

import org.springframework.stereotype.Service;

import com.abhinab.config.AppProperties;
import com.abhinab.service.base.S3BucketService;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

@Service
public class S3BucketServiceImpl implements S3BucketService {

	public static BasicAWSCredentials AWS_CRED = new BasicAWSCredentials(AppProperties.ACCESS_KEY_ID, 
			AppProperties.SECRET_ACCESS_KEY);
	
	@Override
	public AmazonS3 getS3BucketClient() {
		AmazonS3 amazonS3;
		try {
			System.out.println("point 0");
			amazonS3 = AmazonS3ClientBuilder.standard()
							.withCredentials(new AWSStaticCredentialsProvider(AWS_CRED))
							.withRegion(Regions.AF_SOUTH_1)
							.build();
			System.out.println("point 0_1");

		} catch(Exception e) {
			// log Exception
			System.out.println("point 0_1_exception");
			amazonS3 = null;
		} 
		System.out.println("point 0_2");
		return amazonS3;
	}
}
