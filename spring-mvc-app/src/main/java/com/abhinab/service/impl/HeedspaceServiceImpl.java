package com.abhinab.service.impl;

import static com.abhinab.cache.HeedspaceCache.heedspaceEmailTemplatesCahe;
import static com.abhinab.cache.HeedspaceCache.heedspaceConfigsCache;
import static com.abhinab.constants.HeedspaceConstants.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abhinab.bean.HeedspaceEmailTemplate;
import com.abhinab.bean.HeedspaceBlog;
import com.abhinab.bean.HeedspaceConfig;
import com.abhinab.bean.HeedspaceUser;
import com.abhinab.bean.MailDto;
import com.abhinab.builder.HeedspaceBlogBuilder;
import com.abhinab.builder.HeedspaceUserBuilder;
import com.abhinab.cache.HeedspaceCache;
import com.abhinab.dao.base.HeedspaceDao;
import com.abhinab.exception.HeedspaceException;
import com.abhinab.service.base.HeedspaceService;
import com.abhinab.util.JavaMailUtil;

@Service
public class HeedspaceServiceImpl implements HeedspaceService {

	@Autowired
	private HeedspaceDao heedspaceDao;

	@Autowired
	private HeedspaceCache heedspaceCache;

	@Autowired
	private JavaMailUtil javaMailUtil;

	@Override
	public void readHeedspaceUsers() {
		BufferedReader myReader = null;
		List<HeedspaceUser> heedspaceUsers = new ArrayList<>();
		try {
			String fileNameWithPath = heedspaceConfigsCache.get(CONFIG_FOLDER) + heedspaceConfigsCache.get(USERS_FILE);
			File file = new File(fileNameWithPath);
			myReader = new BufferedReader(new FileReader(file));
			String temp;
			while ((temp = myReader.readLine()) != null) {
				int index = 0;
				String data[] = temp.split(";");
				HeedspaceUser hu = new HeedspaceUserBuilder().setFirstName(data[index++]).setLastName(data[index++])
						.setEmail(data[index++]).build();
				heedspaceUsers.add(hu);
			}

			if (!heedspaceUsers.isEmpty()) {
				heedspaceDao.insertHeedspaceUsers(heedspaceUsers);
				this.clearFileData(fileNameWithPath);
			}
		} catch (Exception e) {
			System.err.println(e.getMessage());
			throw new HeedspaceException(e.getMessage());
		} finally {
			try {
				if (myReader != null)
					myReader.close();
			} catch (Exception e) {
				System.err.println(e.getMessage());
			}
		}
	}

	@Override
	public void readHeedspaceBlogs() {
		BufferedReader myReader = null;
		List<HeedspaceBlog> heedspaceBlogs = new ArrayList<>();
		try {
			String fileNameWithPath = heedspaceConfigsCache.get(CONFIG_FOLDER) + heedspaceConfigsCache.get(BLOGS_FILE);
			File file = new File(fileNameWithPath);
			myReader = new BufferedReader(new FileReader(file));
			String temp;
			while ((temp = myReader.readLine()) != null) {
				int index = 0;
				String data[] = temp.split(";");
				HeedspaceBlog hb = new HeedspaceBlogBuilder().setBlogCategory(data[index++]).setBlogName(data[index++])
						.setBlogUrl(data[index++]).build();
				heedspaceBlogs.add(hb);
			}

			if (!heedspaceBlogs.isEmpty()) {
				heedspaceDao.insertHeedspaceBlogs(heedspaceBlogs);
				this.clearFileData(fileNameWithPath);
			}
		} catch (Exception e) {
			System.err.println(e.getMessage());
			throw new HeedspaceException(e.getMessage());
		} finally {
			try {
				if (myReader != null)
					myReader.close();
			} catch (Exception e) {
				System.err.println(e.getMessage());
			}
		}
	}

	@Override
	public void clearFileData(String fileNameWithPath) {
		PrintWriter myWriter = null;
		try {
			myWriter = new PrintWriter(fileNameWithPath);
			myWriter.print("");
		} catch (Exception e) {
			System.err.println(e.getMessage());
			throw new HeedspaceException(e.getMessage());
		} finally {
			try {
				if (myWriter != null)
					myWriter.close();
			} catch (Exception e) {
				System.err.println(e.getMessage());
			}
		}
	}

	@Override
	public void initializeStatusNotification() {
		heedspaceCache.buildCache();
		this.readHeedspaceUsers();
		this.readHeedspaceBlogs();
		this.sendStatusSharingNotification();
	}

	@Override
	public List<HeedspaceEmailTemplate> getHeedspaceEmailTemplates() {
		try {
			return heedspaceDao.findHeedspaceEmailTemplates();
		} catch (Exception e) {
			System.err.println(e.getMessage());
			throw new HeedspaceException(e.getMessage());
		}
	}

	@Override
	public List<HeedspaceConfig> getHeedspaceConfigs() {
		try {
			return heedspaceDao.findHeedspaceConfigs();
		} catch (Exception e) {
			System.err.println(e.getMessage());
			throw new HeedspaceException(e.getMessage());
		}
	}

	@Override
	public Long getMaxIdForHeedspaceBlogs() {
		try {
			return heedspaceDao.findMaxIdForHeedspaceBlogs();
		} catch (Exception e) {
			System.err.println(e.getMessage());
			throw new HeedspaceException(e.getMessage());
		}
	}

	@Override
	public void sendStatusSharingNotification() {
		try {
			long last_blog_shared_id = Long.valueOf(heedspaceConfigsCache.get(LAST_BLOG_SHARED));
			Long max_blog_id = this.getMaxIdForHeedspaceBlogs();
			do {
				if (max_blog_id == null || max_blog_id == new Long(0l)) {
					throw new HeedspaceException("No blogs available to share.");
				}
				long maxId = max_blog_id;

				if (last_blog_shared_id + 1l <= maxId) {
					last_blog_shared_id = last_blog_shared_id + 1;
					heedspaceConfigsCache.put(LAST_BLOG_SHARED, String.valueOf(last_blog_shared_id));
				} else {
					heedspaceConfigsCache.put(LAST_BLOG_SHARED, "0");
					continue;
				}

			} while (false);

			List<HeedspaceUser> heedspaceUsers = this.getHeedspaceUsers();
			HeedspaceBlog hb = this.getHeedspaceBlogByBlogId(last_blog_shared_id).get(0);

			for (HeedspaceUser hu : heedspaceUsers) {
				MailDto mailDto = new MailDto();
				mailDto.setHeedspaceEmailTemplate(heedspaceEmailTemplatesCahe.get(BLOG_SHARE_TEMPLATE));

				Map<String, String> bodyMap = new HashMap<>();
				Map<String, String> subjectMap = new HashMap<>();

				subjectMap.put(SUBJECT, SUBJECT_MESSAGE);
				bodyMap.put(USER_NAME, hu.getFirstName());
				bodyMap.put(BLOG_NAME, hb.getBlogName());
				bodyMap.put(BLOG_URL, hb.getBlogUrl());

				bodyMap.put(FB_SHARE_LINK, this.convertToFbShareableLink(hb.getBlogUrl()));
				bodyMap.put(LI_SHARE_LINK, this.convertToLiShareableLink(hb.getBlogUrl()));

				mailDto.setBodyMap(bodyMap);
				mailDto.setSubjectMap(subjectMap);

				List<String> recipients = new ArrayList<>();
				recipients.add(hu.getEmail());
				mailDto.setRecipients(recipients);
				javaMailUtil.sendMail(mailDto);
			}
			this.updateHeedspaceConfigs(LAST_BLOG_SHARED, String.valueOf(last_blog_shared_id));
		} catch (Exception e) {
			System.err.println(e.getMessage());
			throw new HeedspaceException(e.getMessage());
		}
	}

	@Override
	public List<HeedspaceUser> getHeedspaceUsers() {
		try {
			return heedspaceDao.findHeedspaceUsers();
		} catch (Exception e) {
			System.err.println(e.getMessage());
			throw new HeedspaceException(e.getMessage());
		}
	}

	@Override
	public List<HeedspaceBlog> getHeedspaceBlogByBlogId(long blogId) {
		try {
			return heedspaceDao.findHeedspaceBlogByBlogId(blogId);
		} catch (Exception e) {
			System.err.println(e.getMessage());
			throw new HeedspaceException(e.getMessage());
		}
	}

	@Override
	public String convertToFbShareableLink(String url) {
		try {
			url = url.replaceAll("/", REPLACE_URL_SLASH).replaceAll(":", REPLACE_URL_COLON);
			return heedspaceConfigsCache.get(FB_SHARE).replace(URL, url);
		} catch (Exception e) {
			System.err.println(e.getMessage());
			throw new HeedspaceException(e.getMessage());
		}
	}

	@Override
	public String convertToLiShareableLink(String url) {
		try {
			url = url.replaceAll("/", REPLACE_URL_SLASH).replaceAll(":", REPLACE_URL_COLON);
			return heedspaceConfigsCache.get(LI_SHARE).replace(URL, url);
		} catch (Exception e) {
			System.err.println(e.getMessage());
			throw new HeedspaceException(e.getMessage());
		}
	}
	
	@Override
	public int updateHeedspaceConfigs(String configName, String configValue) {
		try {
			return heedspaceDao.updateHeedspaceConfigs(configName, configValue);
		} catch (Exception e) {
			System.err.println(e.getMessage());
			throw new HeedspaceException(e.getMessage());
		}
	}
}
