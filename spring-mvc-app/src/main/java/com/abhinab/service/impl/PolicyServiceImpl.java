package com.abhinab.service.impl;

import java.io.IOException;

import org.springframework.stereotype.Service;

import com.abhinab.bean.PolicyDocument;
import com.abhinab.config.AppProperties;
import com.abhinab.service.base.PolicyService;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClientBuilder;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class PolicyServiceImpl implements PolicyService {
	
	public static BasicAWSCredentials AWS_CRED = new BasicAWSCredentials(AppProperties.ACCESS_KEY_ID, 
			AppProperties.SECRET_ACCESS_KEY);
	
	@Override
	public String createJsonStringFromClass(PolicyDocument policyDocument) {
		ObjectMapper Obj = new ObjectMapper();
		String jsonStr;
		try { 
			jsonStr = Obj.writeValueAsString(policyDocument); 
		} catch (IOException e) { 
			// log Exception
			jsonStr = null;
		} 
		return jsonStr;
	}
	
	@Override
	public AmazonIdentityManagement getIamClient() {
		AmazonIdentityManagement iam;
		try {
			iam = AmazonIdentityManagementClientBuilder
					.standard().withCredentials(new AWSStaticCredentialsProvider(AWS_CRED))
					.withRegion("\"us-east-1\"")
					.build();

		} catch(Exception e) {
			// log Exception
			iam = null;
		} 
		return iam;
	}
	
}
