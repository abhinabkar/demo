package com.abhinab.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.springframework.stereotype.Service;
import com.abhinab.bean.Todo;

@Service
public class ToDoService {
	private static List<Todo> todos=new ArrayList<Todo>();
	private static int todoCount=3;
	
	static {
		todos.add(new Todo(1, "in28Minutes", "Learn Spring MVC", new Date(),
				0));
		todos.add(new Todo(4, "in28Minutes", "Learn Hibernate", new Date(),
				0));
		todos.add(new Todo(5, "in28Minutes", "Learn Hibernate", new Date(),
				0));
	}
	
	public List<Todo> retrieveToDos(String user)
	{
		List<Todo> filteredTodos=new ArrayList<>();
		for(Todo todo : todos)
		{
			if(todo.getUser().equals(user))
				filteredTodos.add(todo);
		}
		
		return filteredTodos;
	}
	
	public void addToDo(String name, String desc, Date targetDate, int isDone)
	{
		todos.add(new Todo(++todoCount, name, desc, targetDate, isDone));
	}
	
	public void deleteToDo(int id)
	{
		Iterator<Todo> iterator=todos.iterator();
		while(iterator.hasNext()) 
		{
			Todo todo=iterator.next();
			if(id==todo.getId())
				iterator.remove();	
		}
	}
	
	public Todo retrieveTodo(int id) {
		for (Todo todo : todos) {
			if (todo.getId() == id)
				return todo;
		}
		return null;
	}

	public void updateTodo(Todo todo) {
		todos.remove(todo);
		todos.add(todo);
	}
	
}
