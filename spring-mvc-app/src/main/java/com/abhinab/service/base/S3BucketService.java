package com.abhinab.service.base;

import org.springframework.stereotype.Service;

import com.amazonaws.services.s3.AmazonS3;

@Service
public interface S3BucketService {

	AmazonS3 getS3BucketClient();

}
