package com.abhinab.service.base;

import org.springframework.stereotype.Service;

import com.abhinab.bean.PolicyDocument;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;

@Service
public interface PolicyService {

	String createJsonStringFromClass(PolicyDocument policyDocument);

	AmazonIdentityManagement getIamClient();

}
