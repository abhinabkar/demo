package com.abhinab.service.base;

import java.util.List;

import org.springframework.stereotype.Service;

import com.abhinab.bean.HeedspaceBlog;
import com.abhinab.bean.HeedspaceConfig;
import com.abhinab.bean.HeedspaceEmailTemplate;
import com.abhinab.bean.HeedspaceUser;

@Service
public interface HeedspaceService {

	void readHeedspaceUsers();

	void clearFileData(String fileNameWithPath);

	void readHeedspaceBlogs();

	void initializeStatusNotification();

	List<HeedspaceEmailTemplate> getHeedspaceEmailTemplates();

	void sendStatusSharingNotification();

	List<HeedspaceConfig> getHeedspaceConfigs();

	Long getMaxIdForHeedspaceBlogs();

	List<HeedspaceUser> getHeedspaceUsers();

	List<HeedspaceBlog> getHeedspaceBlogByBlogId(long blogId);

	String convertToFbShareableLink(String url);

	String convertToLiShareableLink(String url);

	int updateHeedspaceConfigs(String configName, String configValue);

}
