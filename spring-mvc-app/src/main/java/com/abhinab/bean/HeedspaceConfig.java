package com.abhinab.bean;

import com.abhinab.builder.HeedspaceConfigBuilder;

public class HeedspaceConfig {
	private Long configId;
	private String configName;
	private String configValue;

	public HeedspaceConfig(HeedspaceConfigBuilder builder) {
		this.configId = builder.getConfigId();
		this.configName = builder.getConfigName();
		this.configValue = builder.getConfigValue();
	}
	
	public Long getConfigId() {
		return configId;
	}

	public void setConfigId(Long configId) {
		this.configId = configId;
	}

	public String getConfigName() {
		return configName;
	}

	public void setConfigName(String configName) {
		this.configName = configName;
	}

	public String getConfigValue() {
		return configValue;
	}

	public void setConfigValue(String configValue) {
		this.configValue = configValue;
	}

}
