package com.abhinab.bean;

import java.sql.Timestamp;

import com.abhinab.builder.HeedspaceEmailHistoryBuilder;

public class HeedspaceEmailHistory {
	private Long historyId;
	private Long userId;
	private Long blogId;
	private Short emailSent;
	private Timestamp modifiedOn;
	
	public HeedspaceEmailHistory(HeedspaceEmailHistoryBuilder builder) {
		this.historyId = builder.getHistoryId();
		this.userId = builder.getUserId();
		this.blogId = builder.getBlogId();
		this.emailSent = builder.getEmailSent();
		this.modifiedOn = builder.getModifiedOn();
	}

	public Long getHistoryId() {
		return historyId;
	}

	public void setHistoryId(Long historyId) {
		this.historyId = historyId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getBlogId() {
		return blogId;
	}

	public void setBlogId(Long blogId) {
		this.blogId = blogId;
	}

	public Short getEmailSent() {
		return emailSent;
	}

	public void setEmailSent(Short emailSent) {
		this.emailSent = emailSent;
	}

	public Timestamp getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

}
