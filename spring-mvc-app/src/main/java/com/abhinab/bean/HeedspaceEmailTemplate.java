package com.abhinab.bean;

import java.sql.Timestamp;

import com.abhinab.builder.HeedspaceEmailTemplateBuilder;

public class HeedspaceEmailTemplate {
	private Long templateId;
	private String templateName;
	private String template;
	private Timestamp modifiedOn;

	public HeedspaceEmailTemplate(HeedspaceEmailTemplateBuilder builder) {
		this.templateId = builder.getTemplateId();
		this.templateName = builder.getTemplateName();
		this.template = builder.getTemplate();
		this.modifiedOn = builder.getModifiedOn();
	}
	
	public Long getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Long templateId) {
		this.templateId = templateId;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public Timestamp getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

}
