package com.abhinab.bean;

import java.sql.Timestamp;

import com.abhinab.builder.HeedspaceUserBuilder;

public class HeedspaceUser {
	private Long userId;
	private String firstName;
	private String lastName;
	private Timestamp modifiedOn;
	private String email;
	
	public HeedspaceUser(HeedspaceUserBuilder builder) {
		this.userId = builder.getUserId();
		this.firstName = builder.getFirstName();
		this.lastName = builder.getLastName();
		this.modifiedOn = builder.getModifiedOn();
		this.email = builder.getEmail();
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Timestamp getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
