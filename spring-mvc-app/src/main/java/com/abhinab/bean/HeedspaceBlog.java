package com.abhinab.bean;

import java.sql.Timestamp;

import com.abhinab.builder.HeedspaceBlogBuilder;

public class HeedspaceBlog {
	private Long blogId;
	private String blogName;
	private String blogCategory;
	private String blogUrl;
	private Timestamp modifiedOn;

	public HeedspaceBlog(HeedspaceBlogBuilder builder) {
		this.blogId = builder.getBlogId();
		this.blogName = builder.getBlogName();
		this.blogCategory = builder.getBlogCategory();
		this.blogUrl = builder.getBlogUrl();
		this.modifiedOn = builder.getModifiedOn();
	}
	
	public Long getBlogId() {
		return blogId;
	}

	public void setBlogId(Long blogId) {
		this.blogId = blogId;
	}

	public String getBlogName() {
		return blogName;
	}

	public void setBlogName(String blogName) {
		this.blogName = blogName;
	}

	public String getBlogCategory() {
		return blogCategory;
	}

	public void setBlogCategory(String blogCategory) {
		this.blogCategory = blogCategory;
	}

	public String getBlogUrl() {
		return blogUrl;
	}

	public void setBlogUrl(String blogUrl) {
		this.blogUrl = blogUrl;
	}

	public Timestamp getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

}
