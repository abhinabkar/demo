package com.abhinab.bean;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Statement {
	@JsonProperty("Sid")
	private String sid;
	
	@JsonProperty("Action")
	private List<String> actions;
	
	@JsonProperty("Effect")
	private String effect;
	
	@JsonProperty("Resource")
	private List<String> resources;
	
	public String getSid() {
		return sid;
	}
	public void setSid(String sid) {
		this.sid = sid;
	}
	public List<String> getActions() {
		return actions;
	}
	public void setActions(List<String> actions) {
		this.actions = actions;
	}
	public String getEffect() {
		return effect;
	}
	public void setEffect(String effect) {
		this.effect = effect;
	}
	
	public List<String> getResources() {
		return resources;
	}
	public void setResources(List<String> resources) {
		this.resources = resources;
	}
}
