package com.abhinab.bean;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PolicyDocument {
	@JsonProperty("Version")
	private String version;
	
	@JsonProperty("Statement")
	private List<Statement> statements;
	
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public List<Statement> getStatements() {
		return statements;
	}
	public void setStatements(List<Statement> statements) {
		this.statements = statements;
	}
	
}
