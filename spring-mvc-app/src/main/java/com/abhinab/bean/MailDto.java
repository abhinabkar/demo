package com.abhinab.bean;

import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;

import com.abhinab.constants.MailServerType;

public class MailDto {
	private List<String> recipients;
	private Session session;
	private String accountId;
	private String password;
	private Properties props;
	private Map<String, String> bodyMap;
	private Map<String, String> subjectMap;
	private MailServerType mailServerType;
	private Message message; 
	private HeedspaceEmailTemplate heedspaceEmailTemplate;
	private String monitorEmailId;

	public List<String> getRecipients() {
		return recipients;
	}

	public void setRecipients(List<String> recipients) {
		this.recipients = recipients;
	}

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Properties getProps() {
		return props;
	}

	public void setProps(Properties props) {
		this.props = props;
	}

	public Map<String, String> getBodyMap() {
		return bodyMap;
	}

	public void setBodyMap(Map<String, String> bodyMap) {
		this.bodyMap = bodyMap;
	}

	public Map<String, String> getSubjectMap() {
		return subjectMap;
	}

	public void setSubjectMap(Map<String, String> subjectMap) {
		this.subjectMap = subjectMap;
	}

	public MailServerType getMailServerType() {
		return mailServerType;
	}
	
	public void setMailServerType(MailServerType mailServerType) {
		this.mailServerType = mailServerType;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	public HeedspaceEmailTemplate getHeedspaceEmailTemplate() {
		return heedspaceEmailTemplate;
	}

	public void setHeedspaceEmailTemplate(HeedspaceEmailTemplate heedspaceEmailTemplate) {
		this.heedspaceEmailTemplate = heedspaceEmailTemplate;
	}

	public String getMonitorEmailId() {
		return monitorEmailId;
	}

	public void setMonitorEmailId(String monitorEmailId) {
		this.monitorEmailId = monitorEmailId;
	}
}
