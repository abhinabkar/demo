package com.abhinab.bean;

import java.util.Comparator;

public class NameComparator implements Comparator<Todo> {

	@Override
	public int compare(Todo o1, Todo o2) {
		// TODO Auto-generated method stub
		// HelloWorld

		Todo todo1=(Todo)o1;
		Todo  todo2=(Todo)o2;
		
		
		return todo1.getUser().compareTo(todo2.getUser());
	}

}
