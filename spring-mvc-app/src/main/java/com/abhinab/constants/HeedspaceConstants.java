package com.abhinab.constants;

public class HeedspaceConstants {
	
	// Email template names
	public static final String BLOG_SHARE_TEMPLATE = "BLOG_SHARE_TEMPLATE";
	
	// Template references
	public static final String USER_NAME = "<USER_NAME>";
	public static final String BLOG_NAME = "<BLOG_NAME>";
	public static final String BLOG_URL = "<BLOG_URL>";
	public static final String FB_SHARE_LINK = "<FB_SHARE_LINK>";
	public static final String LI_SHARE_LINK = "<LI_SHARE_LINK>";
	public static final String URL = "<URL>";
	public static final String SUBJECT = "SUBJECT";
	
	// Config names
	public static final String LAST_BLOG_SHARED = "LAST_BLOG_SHARED";
	public static final String EMAIL_ACCOUNT_ID = "EMAIL_ACCOUNT_ID";
	public static final String EMAIL_PASSWORD = "EMAIL_PASSWORD";
	public static final String CONFIG_FOLDER = "CONFIG_FOLDER";
	public static final String USERS_FILE = "USERS_FILE";
	public static final String BLOGS_FILE = "BLOGS_FILE";
	public static final String MONITOR_EMAIL_ID = "MONITOR_EMAIL_ID";
	public static final String FB_SHARE = "FB_SHARE";
	public static final String LI_SHARE = "LI_SHARE";
	
	
	// Heedspace strings
	public static final String SUBJECT_MESSAGE = "Heedspace sharing notification";
	public static final String REPLACE_URL_COLON = "%3A";
	public static final String REPLACE_URL_SLASH = "%2F";
	
	// Exception/Error messages
}
