package com.abhinab.constants;

public enum MailServerType {
	GMAIL("gmail.com"),
	YAHOO("yahoo.com"),
	YAHOOMAIL("yahoomail.com"),
	OUTLOOK("outlook.com"),
	REDIFF("rediff.com"),
	REDIFFMAIL("rediffmail.com");
	
	private String mailServerTypeString;
	
	private MailServerType(String mailServerTypeString) {
		this.mailServerTypeString = mailServerTypeString;
	}

	public String getMailServerTypeString() {
		return mailServerTypeString;
	}
}
