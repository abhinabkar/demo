package com.abhinab.cache;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.abhinab.bean.HeedspaceConfig;
import com.abhinab.bean.HeedspaceEmailTemplate;
import com.abhinab.service.base.HeedspaceService;

@Component
public class HeedspaceCache {

	public static Map<String, HeedspaceEmailTemplate> heedspaceEmailTemplatesCahe = new HashMap<>();
	public static Map<String, String> heedspaceConfigsCache = new HashMap<>();

	@Autowired
	private HeedspaceService heedspaceService;

	public void buildEmailTemplatesCache() {
		try {
			List<HeedspaceEmailTemplate> heedspaceEmailTemplates = heedspaceService.getHeedspaceEmailTemplates();

			if (heedspaceEmailTemplates != null) {
				for (HeedspaceEmailTemplate emailTemplate : heedspaceEmailTemplates) {
					heedspaceEmailTemplatesCahe.put(emailTemplate.getTemplateName(), emailTemplate);
				}
			}
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}
	
	public void buildConfigsCache() {
		try {
			List<HeedspaceConfig> heedspaceConfigs = heedspaceService.getHeedspaceConfigs();

			if (heedspaceConfigs != null) {
				for (HeedspaceConfig configs : heedspaceConfigs) {
					heedspaceConfigsCache.put(configs.getConfigName(), configs.getConfigValue());
				}
			}
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

	public void buildCache() {
		this.buildEmailTemplatesCache();
		this.buildConfigsCache();
	}
}
