package com.abhinab.util;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.abhinab.bean.Todo;
import com.abhinab.controller.PolicyController;
import com.abhinab.controller.S3BucketController;
import com.abhinab.dao.impl.TodoJdbcTemplateDao;
import com.abhinab.service.base.HeedspaceService;
import com.abhinab.service.impl.HeedspaceServiceImpl;

public class MyMain {

	public static void test(String[] args) {
		// TODO Auto-generated method stub

		ApplicationContext ac = new ClassPathXmlApplicationContext("todo-servlet.xml");
		TodoJdbcTemplateDao todoJdbcTemplateDao = ac.getBean("todoJdbcTemplateDao", TodoJdbcTemplateDao.class);
		
		// old Way of Jdbc connectivity
		Todo todo = todoJdbcTemplateDao.getTodo(0);
		if(todo!=null)
			System.out.println(todo.getDesc()+" before "+todo.getTargetDate());
		else
			System.out.println("No Data");
		
		
		// Select operations
		System.out.println(todoJdbcTemplateDao.getTodosCount(1));
		System.out.println(todoJdbcTemplateDao.getTodoById(1));
		System.out.println(todoJdbcTemplateDao.getAllTodos().size());
		System.out.println(todoJdbcTemplateDao.getTodoByIdAndUserUsingNamedJdbcTemplate(2, "george.b"));
		
		// Insert Query
		//todoJdbcTemplateDao.insertTodo(new Todo(2, "george.b", "Cook Food", new Date(), 0));
		System.out.println("After insert, rowCount:"+todoJdbcTemplateDao.getAllTodos().size());
		
		// DDL Statement
		todoJdbcTemplateDao.createTable();
		
	}
	
	public static void test1(String[] args) {
		
		ApplicationContext ac = new ClassPathXmlApplicationContext("todo-servlet.xml");
		try {
		S3BucketController bc = ac.getBean("s3BucketController", S3BucketController.class);
		// PolicyController pc = ac.getBean("policyController", PolicyController.class);
			
		// System.out.println(pc.attachAWSPolicyToUser("arn:aws:iam::262427411851:policy/my_new_policy", 
		//		"project-user-admin"));
		
		// System.out.println(pc.detachAWSPolicyFromUser("arn:aws:iam::262427411851:policy/my_new_policy", 
		// 		"project-user-admin"));
		
		System.out.println(bc.createS3Bucket("abhinab1-s3"));
		
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			((ClassPathXmlApplicationContext) ac).close();
		}
		
	}

	public static void main(String args[]) {
		ApplicationContext ac = new ClassPathXmlApplicationContext("todo-servlet.xml");
		try {
			HeedspaceService heedspaceService = ac.getBean("heedspaceServiceImpl", HeedspaceServiceImpl.class);
			heedspaceService.initializeStatusNotification();
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		} finally {
			((ConfigurableApplicationContext)ac).close();
		}
	}
}
