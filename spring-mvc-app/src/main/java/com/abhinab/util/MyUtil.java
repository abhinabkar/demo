package com.abhinab.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class MyUtil {

	// Constants
	public static String filePath = "<FILE_PATH>";
	public static Integer BATCH_LIST_SIZE = 900;
	
	public static <T> List<List<T>> createAddAndRemoveList(List<T> frontEndList, List<T> backEndList) {
		
		List<T> unchangedList = new ArrayList<T>(frontEndList);
		unchangedList.retainAll(backEndList);
		
		List<T> addList = new ArrayList<T>(frontEndList);
		addList.removeAll(backEndList);
		
		List<T> deleteList = new ArrayList<T>(backEndList);
		deleteList.removeAll(unchangedList);
		
		List<List<T>> list = new ArrayList<>();
		list.add(addList);
		list.add(deleteList);
		list.add(unchangedList);
		
		return list;
	}
	
	/**
	 * Method to check a String is numeric
	 * 
	 * @param strNum
	 * @return
	 * 
	 */
	public static boolean isNumeric(String strNum) {
		if(strNum == null)
			return false;
		
		try {
			Double d = Double.parseDouble(strNum);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}
	
	/**
	 * Write byte stream to a file
	 * 
	 * @param input
	 * @param fileName
	 */
	public static void bytestoPdf(byte[] input, String fileName) {
		OutputStream out;
		try {
			out = new FileOutputStream(filePath+fileName);
			out.write(input);
		} catch(FileNotFoundException e) {
			
		} catch(IOException e) {
			
		} 
	}
	
	/**
	 * Write byte to Excel
	 * 
	 * @param input
	 * @param fileName
	 */
	public static void bytestoExcel(byte[] input, String fileName) {
		InputStream fs = new ByteArrayInputStream(input);
		HSSFWorkbook workBook = null;

		try {
			workBook = new HSSFWorkbook(fs);
			FileOutputStream fileStream;
			final String reportName = filePath+fileName;
			File f = new File(reportName);
			
			fileStream = new FileOutputStream(f);
			workBook.write(fileStream);
			fileStream.close();
			
		} catch (FileNotFoundException e) {
			// TODO: handle exception
		} catch (IOException e) {
			// TODO: handle exception
		}
	}
	
	/**
	 * Use to convert a file to byte stream
	 * 
	 * @param fileName
	 * @return
	 */
	public static byte[] excelTobytes(String fileName) {
		File inputFile = new File(filePath+fileName);
		FileInputStream inputStream = null;
		byte[] fileBytes = null;
		
		try {
			inputStream = new FileInputStream(inputFile);
			fileBytes = new byte[(int) inputFile.length()];
			inputStream.read(fileBytes);
		} catch (FileNotFoundException e) {
			// TODO: handle exception
		} catch (IOException e) {
			// TODO: handle exception
		} finally {
			if(inputStream != null)
				try {
					inputStream.close();
				} catch (IOException e) {
					// TODO: handle exception
				}
		}
		return fileBytes;
	}
	
	/**
	 * Utility to convert BigDecimal to Integer
	 * 
	 * @param bigDecimal
	 * @return
	 */
	public static Integer bigDecimalToInteger(BigDecimal bigDecimal) {
		if(bigDecimal == null)
			return null;
		return bigDecimal.intValueExact();
	}
	
	public static <T> List<Set<T>> getBatchLimitedList(Set<T> input) {
		List<Set<T>> listResult = new ArrayList<>();
		if(MyUtil.isNullOrEmpty(input))
			return listResult;
		
		if(input.size() <= BATCH_LIST_SIZE) {
			listResult.add(input);
		} else {
			Iterator<T> iterator = input.iterator();
			int counter = 0;
			Set<T> toBeAddedList = new HashSet<>();
			
			while(iterator.hasNext()) {
				if(counter == 0)
					toBeAddedList = new HashSet<>(BATCH_LIST_SIZE);
				
				toBeAddedList.add(iterator.next());
				++counter;
				
				if(counter == BATCH_LIST_SIZE) {
					counter = 0;
					listResult.add(toBeAddedList);
				}
			}
			
			if(input.size() % BATCH_LIST_SIZE != 0) {
				listResult.add(toBeAddedList);
			}
		}
		return listResult;
	}
	
	public static boolean isNullOrEmpty(String input) {
		return input==null || input.length() == 0;
	}
	
	public static boolean isNullOrZero(Integer input) {
		return input == null || input == 0;
	}
	
	public static <T> boolean isNullOrEmpty(Collection<T> list) {
		return list == null || list.isEmpty();
	}
	
	public static boolean isNullOrEmpty(Map<?, ?> map) {
		return map == null || map.isEmpty();
	}
}
