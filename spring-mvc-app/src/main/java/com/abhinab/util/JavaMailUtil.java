package com.abhinab.util;

import static com.abhinab.cache.HeedspaceCache.heedspaceConfigsCache;

import static com.abhinab.constants.HeedspaceConstants.*;

import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.stereotype.Component;

import com.abhinab.bean.MailDto;
import com.abhinab.config.AppProperties;
import com.abhinab.constants.MailServerType;

@Component
public class JavaMailUtil {

	public void sendMail(MailDto mailDto) throws Exception {

		// mailDto.setAccountId(heedspaceConfigsCache.get(EMAIL_ACCOUNT_ID));
		// mailDto.setPassword(heedspaceConfigsCache.get(EMAIL_PASSWORD));
		mailDto.setAccountId(heedspaceConfigsCache.get(EMAIL_ACCOUNT_ID));
		mailDto.setPassword(heedspaceConfigsCache.get(EMAIL_PASSWORD));

		this.findMailServerType(mailDto);
		this.findSmtpMailDetails(mailDto);

		Session session = Session.getInstance(mailDto.getProps(), new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(mailDto.getAccountId(), mailDto.getPassword());
			}
		});
		mailDto.setSession(session);

		this.prepareMessage(mailDto);
		Transport.send(mailDto.getMessage());
		Logger.getLogger(JavaMailUtil.class.getName()).log(Level.INFO,
				"Email sent successfully to " + mailDto.getRecipients().get(0));
	}

	public void findMailServerType(MailDto mailDto) throws Exception {
		String pattern = ".*";
		if (mailDto.getAccountId().matches(pattern + MailServerType.GMAIL.getMailServerTypeString())) {
			mailDto.setMailServerType(MailServerType.GMAIL);
		} else if (mailDto.getAccountId().matches(pattern + MailServerType.YAHOO.getMailServerTypeString())
				|| mailDto.getAccountId().matches(pattern + MailServerType.YAHOOMAIL.getMailServerTypeString())) {
			mailDto.setMailServerType(MailServerType.YAHOO);
		} else if (mailDto.getAccountId().matches(pattern + MailServerType.REDIFF.getMailServerTypeString())
				|| mailDto.getAccountId().matches(pattern + MailServerType.REDIFFMAIL.getMailServerTypeString())) {
			mailDto.setMailServerType(MailServerType.REDIFF);
		} else if (mailDto.getAccountId().matches(pattern + MailServerType.OUTLOOK.getMailServerTypeString())) {
			mailDto.setMailServerType(MailServerType.OUTLOOK);
		} else {
			throw new Exception("TO mail account address not supported.");
		}
	}

	public void prepareMessage(MailDto mailDto) {
		try {
			Message message = new MimeMessage(mailDto.getSession());
			message.setFrom(new InternetAddress(mailDto.getAccountId()));
			InternetAddress[] addresses = new InternetAddress[mailDto.getRecipients().size()];
			int index = 0;
			for (String recipient : mailDto.getRecipients()) {
				addresses[index++] = new InternetAddress(recipient);
			}
			message.setRecipients(Message.RecipientType.TO, addresses);
			message.setRecipient(Message.RecipientType.BCC,
					new InternetAddress(heedspaceConfigsCache.get(MONITOR_EMAIL_ID)));
			message.setSubject(mailDto.getSubjectMap().get(SUBJECT));
			message.setContent(getHtmlMessageFromEmailTemplate(mailDto), "text/html");
			mailDto.setMessage(message);
		} catch (Exception e) {
			Logger.getLogger(JavaMailUtil.class.getName()).log(Level.WARNING, "Mail message creation error");
		}
	}

	public void findSmtpMailDetails(MailDto mailDto) throws Exception {
		Properties props = new Properties();

		switch (mailDto.getMailServerType()) {
		case GMAIL:
			props.put("mail.smtp.host", "smtp.gmail.com"); // SMTP Host
			props.put("mail.smtp.port", "587"); // TLS Port
			props.put("mail.smtp.auth", "true"); // enable authentication
			props.put("mail.smtp.starttls.enable", "true");
			break;
		case YAHOO:
			props.put("mail.smtp.host", "smtp.mail.yahoo.com"); // SMTP Host
			props.put("mail.smtp.port", "587"); // TLS Port
			props.put("mail.smtp.auth", "true"); // enable authentication
			props.put("mail.smtp.starttls.enable", "true");
			break;
		case OUTLOOK:
			props.put("mail.smtp.host", "smtp-mail.outlook.com"); // SMTP Host
			props.put("mail.smtp.port", "587"); // TLS Port
			props.put("mail.smtp.auth", "true"); // enable authentication
			props.put("mail.smtp.starttls.enable", "true");
			break;
		case REDIFF:
			props.put("mail.smtp.host", "smtp.rediffmail.com"); // SMTP Host
			props.put("mail.smtp.port", "587"); // TLS Port
			props.put("mail.smtp.auth", "true"); // enable authentication
			props.put("mail.smtp.starttls.enable", "true");
			break;
		default:
			throw new Exception("TO mail account address not supported.");
		}
		mailDto.setProps(props);
	}

	public String getHtmlMessageFromEmailTemplate(MailDto mailDto) {
		String htmlText = mailDto.getHeedspaceEmailTemplate().getTemplate();
		for (Map.Entry<String, String> map : mailDto.getBodyMap().entrySet()) {
			htmlText = htmlText.replaceAll(map.getKey(), map.getValue());
		}
		return htmlText;
	}
}
