package com.abhinab.beanentity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name="todo")
public class TodoEntity 
{	
	@Id
	@Column(name="id")
	private int id;
	@Column(name="username")
	private String user;
	@Column(name="description")
	private String desc;
	@Temporal(TemporalType.DATE)
	@Column(name="targetdate")
	private Date targetDate;
	@Column(name="isdone")
	private int isDone;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public Date getTargetDate() {
		return targetDate;
	}
	public void setTargetDate(Date targetDate) {
		this.targetDate = targetDate;
	}
	public int getIsDone() {
		return isDone;
	}
	public void setIsDone(int isDone) {
		this.isDone = isDone;
	}
	

}
