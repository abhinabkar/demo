package com.abhinab.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.abhinab.bean.HeedspaceUser;
import com.abhinab.builder.HeedspaceUserBuilder;

public class HeedspaceUserRowMapper implements RowMapper<HeedspaceUser> {

	@Override
	public HeedspaceUser mapRow(ResultSet rs, int arg1) throws SQLException {
		int index = 1;
		return new HeedspaceUserBuilder().setUserId(rs.getLong(index++)).setFirstName(rs.getString(index++))
				.setLastName(rs.getString(index++)).setEmail(rs.getString(index++))
				.setModifiedOn(rs.getTimestamp(index)).build();
	}
}
