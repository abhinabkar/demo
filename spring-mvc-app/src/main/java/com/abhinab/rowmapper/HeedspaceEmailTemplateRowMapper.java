package com.abhinab.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.abhinab.bean.HeedspaceEmailTemplate;
import com.abhinab.builder.HeedspaceEmailTemplateBuilder;

public class HeedspaceEmailTemplateRowMapper implements RowMapper<HeedspaceEmailTemplate> {

	@Override
	public HeedspaceEmailTemplate mapRow(ResultSet rs, int arg1) throws SQLException {
		int index = 1;
		return new HeedspaceEmailTemplateBuilder().setTemplateId(rs.getLong(index++))
				.setTemplateName(rs.getString(index++)).setTemplate(rs.getString(index++))
				.setModifiedOn(rs.getTimestamp(index)).build();
	}

}
