package com.abhinab.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.abhinab.bean.HeedspaceEmailHistory;
import com.abhinab.builder.HeedspaceEmailHistoryBuilder;

public class HeedspaceEmailHistoryRowMapper implements RowMapper<HeedspaceEmailHistory> {

	@Override
	public HeedspaceEmailHistory mapRow(ResultSet rs, int arg1) throws SQLException {
		int index = 1;
		return new HeedspaceEmailHistoryBuilder().setHistoryId(rs.getLong(index++)).setUserId(rs.getLong(index++))
				.setBlogId(rs.getLong(index++)).setEmailSent(rs.getShort(index++)).setModifiedOn(rs.getTimestamp(index))
				.build();
	}

}
