package com.abhinab.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.abhinab.bean.HeedspaceBlog;
import com.abhinab.builder.HeedspaceBlogBuilder;

public class HeedspaceBlogRowMapper implements RowMapper<HeedspaceBlog> {

	@Override
	public HeedspaceBlog mapRow(ResultSet rs, int arg1) throws SQLException {
		int index = 1;
		return new HeedspaceBlogBuilder().setBlogId(rs.getLong(index++)).setBlogName(rs.getString(index++))
				.setBlogCategory(rs.getString(index++)).setBlogUrl(rs.getString(index++))
				.setModifiedOn(rs.getTimestamp(index)).build();
	}
}
