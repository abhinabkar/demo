package com.abhinab.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.abhinab.bean.HeedspaceConfig;
import com.abhinab.builder.HeedspaceConfigBuilder;

public class HeedspaceConfigRowMapper implements RowMapper<HeedspaceConfig> {

	@Override
	public HeedspaceConfig mapRow(ResultSet rs, int arg1) throws SQLException {
		int index = 1;
		return new HeedspaceConfigBuilder().setConfigId(rs.getLong(index++)).setConfigName(rs.getString(index++))
				.setConfigValue(rs.getString(index)).build();
	}

}
