package com.abhinab.exception;

public class HeedspaceException extends RuntimeException {
	
	private static final long serialVersionUID = -5387772153464498488L;

	public HeedspaceException(String message) {
		super(message);
	}
}
