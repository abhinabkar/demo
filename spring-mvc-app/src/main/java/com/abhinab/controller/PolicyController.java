package com.abhinab.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abhinab.bean.PolicyDocument;
import com.abhinab.bean.Statement;
import com.abhinab.service.base.PolicyService;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.model.AttachGroupPolicyRequest;
import com.amazonaws.services.identitymanagement.model.AttachRolePolicyRequest;
import com.amazonaws.services.identitymanagement.model.AttachUserPolicyRequest;
import com.amazonaws.services.identitymanagement.model.AttachedPolicy;
import com.amazonaws.services.identitymanagement.model.CreatePolicyRequest;
import com.amazonaws.services.identitymanagement.model.CreatePolicyResult;
import com.amazonaws.services.identitymanagement.model.DeletePolicyRequest;
import com.amazonaws.services.identitymanagement.model.DeletePolicyResult;
import com.amazonaws.services.identitymanagement.model.DetachGroupPolicyRequest;
import com.amazonaws.services.identitymanagement.model.DetachRolePolicyRequest;
import com.amazonaws.services.identitymanagement.model.DetachUserPolicyRequest;
import com.amazonaws.services.identitymanagement.model.ListAttachedGroupPoliciesRequest;
import com.amazonaws.services.identitymanagement.model.ListAttachedGroupPoliciesResult;
import com.amazonaws.services.identitymanagement.model.ListAttachedRolePoliciesRequest;
import com.amazonaws.services.identitymanagement.model.ListAttachedRolePoliciesResult;
import com.amazonaws.services.identitymanagement.model.ListAttachedUserPoliciesRequest;
import com.amazonaws.services.identitymanagement.model.ListAttachedUserPoliciesResult;

@RestController
@RequestMapping("/policyapp")
public class PolicyController {
	
	@Autowired
	PolicyService policyService;
	
	@RequestMapping("/policy/create") 
	public String createAWSPolicy(@RequestParam String policyName, 
								  @RequestBody PolicyDocument policyDocument) {
		String returnMessage = "";
		try {
			AmazonIdentityManagement iam = policyService.getIamClient();
			String policyDocJsonString = policyService.createJsonStringFromClass(policyDocument);
			CreatePolicyRequest request = new CreatePolicyRequest()
					.withPolicyName(policyName)
					.withPolicyDocument(policyDocJsonString);
																
			CreatePolicyResult response = iam.createPolicy(request);
			returnMessage = "Policy created with name: " + response.getPolicy().getPolicyName();
		} catch(Exception e) {
			// Log Exception
			returnMessage = e.getMessage();
		}
		return returnMessage;
	}
	
	@RequestMapping("/policy/delete") 
	public String createAWSPolicy(@RequestParam String policyArn) {
		String returnMessage = "";
		try {
			AmazonIdentityManagement iam = policyService.getIamClient();
			DeletePolicyRequest request = new DeletePolicyRequest()
					.withPolicyArn(policyArn);
			DeletePolicyResult response = iam.deletePolicy(request);
			returnMessage = "Policy deleted: " + policyArn;
		} catch(Exception e) {
			// Log Exception
			returnMessage = e.getMessage();
		}
		return returnMessage;
	}
	
	@RequestMapping("/policy/attach/user") 
	public String attachAWSPolicyToUser(@RequestParam String policyArn, 
										@RequestParam String userName) {
		String returnMessage;
		try {
			AmazonIdentityManagement iam = policyService.getIamClient();
			ListAttachedUserPoliciesRequest request =
					new ListAttachedUserPoliciesRequest()
						.withUserName(userName);
			List<AttachedPolicy> matchingPolicies = new ArrayList<>();
			
			boolean done = false;
			while(!done) {
				ListAttachedUserPoliciesResult response =
						iam.listAttachedUserPolicies(request);

	            matchingPolicies.addAll(
	                    response.getAttachedPolicies()
	                            .stream()
	                            .filter(p -> p.getPolicyArn().equals(policyArn))
	                            .collect(Collectors.toList()));

	            if(!response.getIsTruncated()) {
	                done = true;
	            }
	            request.setMarker(response.getMarker());
	        }
			
			if (matchingPolicies.size() > 0) {
				return matchingPolicies.get(0).getPolicyName()
	                    + " policy is already attached to this user " + userName;
	        }
			
			AttachUserPolicyRequest attachRequest = new AttachUserPolicyRequest()
					.withUserName(userName)
					.withPolicyArn(policyArn);

		    iam.attachUserPolicy(attachRequest);
		    returnMessage = "Successfully attached policy " + policyArn 
	                			+ " to user " + userName;
			
		} catch(Exception e) {
			// Log Exception
			returnMessage = e.getMessage();
		}
		return returnMessage;
	}
	
	@RequestMapping("/policy/attach/role") 
	public String attachAWSPolicyToRole(@RequestParam String policyArn, 
										@RequestParam String roleName) {
		String returnMessage;
		try {
			AmazonIdentityManagement iam = policyService.getIamClient();
			ListAttachedRolePoliciesRequest request =
		            new ListAttachedRolePoliciesRequest()
		                .withRoleName(roleName);
			
			List<AttachedPolicy> matchingPolicies = new ArrayList<>();
			
			boolean done = false;

			while(!done) {
				ListAttachedRolePoliciesResult response =
						iam.listAttachedRolePolicies(request);

	            matchingPolicies.addAll(
	                    response.getAttachedPolicies()
	                            .stream()
	                            .filter(p -> p.getPolicyArn().equals(policyArn))
	                            .collect(Collectors.toList()));

	            if(!response.getIsTruncated()) {
	                done = true;
	            }
	            request.setMarker(response.getMarker());
	        }
			
			if (matchingPolicies.size() > 0) {
				return matchingPolicies.get(0).getPolicyName()
	                    + " policy is already attached to this role.";
	        }
			
			AttachRolePolicyRequest attachRequest =new AttachRolePolicyRequest()
					.withRoleName(roleName)
					.withPolicyArn(policyArn);

		    iam.attachRolePolicy(attachRequest);
		    returnMessage = "Successfully attached policy " + policyArn 
	                			+ " to role " + roleName;
		} catch(Exception e) {
			// Log Exception
			returnMessage = e.getMessage();
		}
		
		return returnMessage;
	}
	
	@RequestMapping("/policy/attach/group") 
	public String attachAWSPolicyToGroup(@RequestParam String policyArn, 
										@RequestParam String groupName) {
		String returnMessage;
		try {
			AmazonIdentityManagement iam = policyService.getIamClient();
			ListAttachedGroupPoliciesRequest request =
					new ListAttachedGroupPoliciesRequest()
						.withGroupName(groupName);
			List<AttachedPolicy> matchingPolicies = new ArrayList<>();
			
			boolean done = false;
			while(!done) {
				ListAttachedGroupPoliciesResult response =
						iam.listAttachedGroupPolicies(request);

	            matchingPolicies.addAll(
	                    response.getAttachedPolicies()
	                            .stream()
	                            .filter(p -> p.getPolicyArn().equals(policyArn))
	                            .collect(Collectors.toList()));

	            if(!response.getIsTruncated()) {
	                done = true;
	            }
	            request.setMarker(response.getMarker());
	        }
			
			if (matchingPolicies.size() > 0) {
				return matchingPolicies.get(0).getPolicyName()
	                    + " policy is already attached to this Group " + groupName;
	        }
			
			AttachGroupPolicyRequest attachRequest = new AttachGroupPolicyRequest()
					.withGroupName(groupName)
					.withPolicyArn(policyArn);

		    iam.attachGroupPolicy(attachRequest);
		    returnMessage = "Successfully attached policy " + policyArn 
	                			+ " to Group " + groupName;
			
		} catch(Exception e) {
			// Log Exception
			returnMessage = e.getMessage();
		}
		return returnMessage;
	}

	
	@RequestMapping("/policy/detach/user") 
	public String detachAWSPolicyFromUser(@RequestParam String policyArn, 
										@RequestParam String userName) {
		String returnMessage;
		try {
			AmazonIdentityManagement iam = policyService.getIamClient();
			
			DetachUserPolicyRequest detachRequest = new DetachUserPolicyRequest()
					.withUserName(userName)
					.withPolicyArn(policyArn);

		    iam.detachUserPolicy(detachRequest);
		    returnMessage = "Successfully detached policy " + policyArn 
	                			+ " from user " + userName;
			
		} catch(Exception e) {
			// Log Exception
			returnMessage = e.getMessage();
		}
		return returnMessage;
	}

	@RequestMapping("/policy/detach/role") 
	public String detachAWSPolicyFromRole(@RequestParam String policyArn, 
										@RequestParam String roleName) {
		String returnMessage;
		try {
			AmazonIdentityManagement iam = policyService.getIamClient();
			
			DetachRolePolicyRequest detachRequest = new DetachRolePolicyRequest()
					.withRoleName(roleName)
					.withPolicyArn(policyArn);

		    iam.detachRolePolicy(detachRequest);
		    returnMessage = "Successfully detached policy " + policyArn 
	                			+ " from role " + roleName;
			
		} catch(Exception e) {
			// Log Exception
			returnMessage = e.getMessage();
		}
		return returnMessage;
	}
	
	@RequestMapping("/policy/detach/group") 
	public String detachAWSPolicyFromGroup(@RequestParam String policyArn, 
										@RequestParam String groupName) {
		String returnMessage;
		try {
			AmazonIdentityManagement iam = policyService.getIamClient();
			
			DetachGroupPolicyRequest detachRequest = new DetachGroupPolicyRequest()
					.withGroupName(groupName)
					.withPolicyArn(policyArn);

		    iam.detachGroupPolicy(detachRequest);
		    returnMessage = "Successfully detached policy " + policyArn 
	                			+ " from Group " + groupName;
			
		} catch(Exception e) {
			// Log Exception
			returnMessage = e.getMessage();
		}
		return returnMessage;
	}
	
	public static void main(String args[]) {
		
		List<String> resources = new ArrayList<>();
		resources.add("resource1");
		resources.add("resource2");
		resources.add("resource3");
		
		List<String> actions = new ArrayList<>();
		actions.add("logs:CreateLogGroup");
		
		Statement statement = new Statement();
		statement.setActions(actions);
		statement.setResources(resources);
		statement.setSid("sid1");
		statement.setEffect("Allow");
		
		List<String> resources1 = new ArrayList<>();
		resources1.add("resource1");
		
		List<String> actions1 = new ArrayList<>();
		actions1.add("logs:CreateLogGroup");
		actions1.add("s3:bucket");
		
		Statement statement1 = new Statement();
		statement1.setActions(actions1);
		statement1.setResources(resources1);
		statement1.setSid("sid2");
		statement1.setEffect("Deny");
		
		List<Statement> statements = new ArrayList<>();
		statements.add(statement);
		statements.add(statement1);
		
		PolicyDocument policyDocument = new PolicyDocument();
		policyDocument.setStatements(statements);
		policyDocument.setVersion("2012-10-17");
		
	}
}
