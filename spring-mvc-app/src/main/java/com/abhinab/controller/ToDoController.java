package com.abhinab.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import com.abhinab.bean.Todo;
import com.abhinab.service.ToDoService;

@Controller
@SessionAttributes("name")
public class ToDoController {
	
	@Autowired
	private ToDoService toDoService;
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				dateFormat, false));
	}
	
	@RequestMapping(value="/list-todos", method=RequestMethod.GET)
	public String showToDos(ModelMap model)
	{
		model.put("todos", toDoService.retrieveToDos("in28Minutes"));
		return "list-todos";
	}
	
	@RequestMapping(value="/add-todo", method=RequestMethod.GET)
	public String showToDoPage(ModelMap model)
	{
		model.put("todo", new Todo(0, "in28Minutes", "Learn Cycling", new Date(), 0));
		return "todo";
	}
	
	@RequestMapping(value="/add-todo", method=RequestMethod.POST)
	public String addToDo(@RequestParam String desc, ModelMap model, @Valid Todo todo, BindingResult result)
	{	
		if(result.hasErrors())
		{
			model.put("todo", todo);
			return "todo";
		}
		
		toDoService.addToDo("in28Minutes", desc, new Date(), 0);
		model.clear();
		return "redirect:list-todos";
	}
	
	@RequestMapping(value="/update-todo", method=RequestMethod.GET)
	public String updateToDo(ModelMap model, @RequestParam int id)
	{
		model.clear();
		Todo todo=toDoService.retrieveTodo(id);
		model.addAttribute("todo", todo);
		return "todo";
	}
	
	@RequestMapping(value="/update-todo", method=RequestMethod.POST)
	public String updateToDo(@Valid Todo todo, BindingResult result, ModelMap model)
	{
		if(result.hasErrors())
		{
			model.put("todo", todo);
			return "todo";
		}
		
		toDoService.updateTodo(todo);
		return "redirect:list-todos";
	}
	
	
	@RequestMapping(value="/delete-todo", method=RequestMethod.GET)
	public String deleteToDo(ModelMap model, @RequestParam int id)
	{
		model.clear();
		toDoService.deleteToDo(id);
		return "redirect:list-todos";
	}
}
