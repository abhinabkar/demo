package com.abhinab.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abhinab.service.base.S3BucketService;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.Bucket;


@RestController
@RequestMapping("/s3bucketapp")
public class S3BucketController {

	@Autowired
	private S3BucketService s3BucketService;
	
	@RequestMapping("/create")
	public String createS3Bucket(@RequestParam String bucketName) {
		String returnMessage;
		
		try {
			final AmazonS3 amazonS3 = s3BucketService.getS3BucketClient();
			if(amazonS3.doesBucketExistV2(bucketName))
				return "S3 Bucket already exists with name: " + bucketName;
			
			Bucket bucket = amazonS3.createBucket(bucketName);
			returnMessage = "S3 Bucket created successfully with name: " + bucket.getName();
		} catch(Exception e) {
			// Log exception
			returnMessage = e.getMessage();
		}
		
		return returnMessage;
	}
	
	@RequestMapping("/delete")
	public String deleteS3Bucket(@RequestParam String bucketName) {
		String returnMessage;
		
		try {
			final AmazonS3 amazonS3 = s3BucketService.getS3BucketClient();
			if(!amazonS3.doesBucketExistV2(bucketName))
				return "S3 Bucket does not exist with name: " + bucketName;
			
			amazonS3.deleteBucket(bucketName);
			returnMessage = "S3 Bucket deleted successfully with name: " + bucketName;
		} catch(Exception e) {
			// Log exception
			returnMessage = e.getMessage();
		}
		
		return returnMessage;
	}
	
	@RequestMapping("/modify")
	public String modifyS3Bucket(@RequestParam String bucketName) {
		String returnMessage;
		
		try {
			final AmazonS3 amazonS3 = s3BucketService.getS3BucketClient();
			if(!amazonS3.doesBucketExistV2(bucketName))
				return "S3 Bucket does not exist with name: " + bucketName;
			
			Bucket namedBucket = null;
			System.out.println("Point 1");
	        List<Bucket> buckets = amazonS3.listBuckets();
	        for (Bucket b : buckets) {
	        	System.out.println(b.getName());
	            if (b.getName().equals(bucketName)) {
	            	namedBucket = b;
	            }
	        }
	        
	        returnMessage = "Successfully fetched bucket with name: " + namedBucket.getName();
	        
		} catch(Exception e) {
			// Log exception
			returnMessage = e.getMessage();
		}
		
		return returnMessage;
	}
}
