package com.abhinab.builder;

import java.sql.Timestamp;

import com.abhinab.bean.HeedspaceBlog;

public class HeedspaceBlogBuilder {
	private Long blogId;
	private String blogName;
	private String blogCategory;
	private String blogUrl;
	private Timestamp modifiedOn;

	public Long getBlogId() {
		return blogId;
	}

	public HeedspaceBlogBuilder setBlogId(Long blogId) {
		this.blogId = blogId;
		return this;
	}

	public String getBlogName() {
		return blogName;
	}

	public HeedspaceBlogBuilder setBlogName(String blogName) {
		this.blogName = blogName;
		return this;
	}

	public String getBlogCategory() {
		return blogCategory;
	}

	public HeedspaceBlogBuilder setBlogCategory(String blogCategory) {
		this.blogCategory = blogCategory;
		return this;
	}

	public String getBlogUrl() {
		return blogUrl;
	}

	public HeedspaceBlogBuilder setBlogUrl(String blogUrl) {
		this.blogUrl = blogUrl;
		return this;
	}

	public Timestamp getModifiedOn() {
		return modifiedOn;
	}

	public HeedspaceBlogBuilder setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
		return this;
	}

	public HeedspaceBlog build() {
		return new HeedspaceBlog(this);
	}
}
