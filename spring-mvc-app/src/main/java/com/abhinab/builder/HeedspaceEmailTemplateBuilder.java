package com.abhinab.builder;

import java.sql.Timestamp;

import com.abhinab.bean.HeedspaceEmailTemplate;

public class HeedspaceEmailTemplateBuilder {
	private Long templateId;
	private String templateName;
	private String template;
	private Timestamp modifiedOn;

	public Long getTemplateId() {
		return templateId;
	}

	public HeedspaceEmailTemplateBuilder setTemplateId(Long templateId) {
		this.templateId = templateId;
		return this;
	}

	public String getTemplateName() {
		return templateName;
	}

	public HeedspaceEmailTemplateBuilder setTemplateName(String templateName) {
		this.templateName = templateName;
		return this;
	}

	public String getTemplate() {
		return template;
	}

	public HeedspaceEmailTemplateBuilder setTemplate(String template) {
		this.template = template;
		return this;
	}

	public Timestamp getModifiedOn() {
		return modifiedOn;
	}

	public HeedspaceEmailTemplateBuilder setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
		return this;
	}

	public HeedspaceEmailTemplate build() {
		return new HeedspaceEmailTemplate(this);
	}
}
