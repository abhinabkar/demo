package com.abhinab.builder;

import java.sql.Timestamp;

import com.abhinab.bean.HeedspaceEmailHistory;

public class HeedspaceEmailHistoryBuilder {
	private Long historyId;
	private Long userId;
	private Long blogId;
	private Short emailSent;
	private Timestamp modifiedOn;

	public Long getHistoryId() {
		return historyId;
	}

	public HeedspaceEmailHistoryBuilder setHistoryId(Long historyId) {
		this.historyId = historyId;
		return this;
	}

	public Long getUserId() {
		return userId;
	}

	public HeedspaceEmailHistoryBuilder setUserId(Long userId) {
		this.userId = userId;
		return this;
	}

	public Long getBlogId() {
		return blogId;
	}

	public HeedspaceEmailHistoryBuilder setBlogId(Long blogId) {
		this.blogId = blogId;
		return this;
	}

	public Short getEmailSent() {
		return emailSent;
	}

	public HeedspaceEmailHistoryBuilder setEmailSent(Short emailSent) {
		this.emailSent = emailSent;
		return this;
	}

	public Timestamp getModifiedOn() {
		return modifiedOn;
	}

	public HeedspaceEmailHistoryBuilder setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
		return this;
	}
	
	public HeedspaceEmailHistory build() {
		return new HeedspaceEmailHistory(this);
	}
}
