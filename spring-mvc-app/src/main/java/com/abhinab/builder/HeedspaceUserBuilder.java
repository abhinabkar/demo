package com.abhinab.builder;

import java.sql.Timestamp;

import com.abhinab.bean.HeedspaceUser;

public class HeedspaceUserBuilder {
	private Long userId;
	private String firstName;
	private String lastName;
	private Timestamp modifiedOn;
	private String email;

	public Long getUserId() {
		return userId;
	}

	public HeedspaceUserBuilder setUserId(Long userId) {
		this.userId = userId;
		return this;
	}

	public String getFirstName() {
		return firstName;
	}

	public HeedspaceUserBuilder setFirstName(String firstName) {
		this.firstName = firstName;
		return this;
	}

	public String getLastName() {
		return lastName;
	}

	public HeedspaceUserBuilder setLastName(String lastName) {
		this.lastName = lastName;
		return this;
	}

	public Timestamp getModifiedOn() {
		return modifiedOn;
	}

	public HeedspaceUserBuilder setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
		return this;
	}

	public String getEmail() {
		return email;
	}

	public HeedspaceUserBuilder setEmail(String email) {
		this.email = email;
		return this;
	}

	public HeedspaceUser build() {
		return new HeedspaceUser(this);
	}
}
