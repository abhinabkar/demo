package com.abhinab.builder;

import com.abhinab.bean.HeedspaceConfig;

public class HeedspaceConfigBuilder {
	private Long configId;
	private String configName;
	private String configValue;

	public Long getConfigId() {
		return configId;
	}

	public HeedspaceConfigBuilder setConfigId(Long configId) {
		this.configId = configId;
		return this;
	}

	public String getConfigName() {
		return configName;
	}

	public HeedspaceConfigBuilder setConfigName(String configName) {
		this.configName = configName;
		return this;
	}

	public String getConfigValue() {
		return configValue;
	}

	public HeedspaceConfigBuilder setConfigValue(String configValue) {
		this.configValue = configValue;
		return this;
	}

	public HeedspaceConfig build() {
		return new HeedspaceConfig(this);
	}
}
