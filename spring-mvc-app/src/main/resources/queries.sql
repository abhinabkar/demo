

select * from todo;
select count(*) from todo;
drop table todo;

CREATE TABLE todo(
       id          NUMBER(6) PRIMARY KEY,
       username    VARCHAR2(10) NOT NULL,
       description VARCHAR2(40),
       targetdate  DATE,
       isdone VARCHAR2(5) check(isdone IN ('true', 'false')) NOT NULL
);

/* Insert Data */
INSERT INTO todo values(1, 'admin', 'To Complete Core Java', SYSDATE, 'false')

commit;

/* Practice commands */
ALTER TABLE todo MODIFY id NUMBER(6,0)
ALTER TABLE todo MODIFY username NOT NULL
ALTER TABLE todo MODIFY description VARCHAR2(30)

ALTER TABLE todo ADD COLUMN user VARCHAR(6);

desc todo

DROP TABLE todo