import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.abhinab.bean.NameComparator;
import com.abhinab.bean.Todo;
import com.abhinab.dao.impl.TodoRepository;
import com.abhinab.service.ToDoService;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Test test=new Test();
		Todo todo=new Todo(5, "in28Minutes", "Learn Hibernate", new Date(),
				0);
		
		test.addTodos(todo);
	}
	
	public void addTodos(Todo todo)
	{
		try {
			new TodoRepository().addTodos(todo);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
	}
	
	public void sortTodos()
	{
	ToDoService todoService=new ToDoService();
		
		List<Todo> listTodo=todoService.retrieveToDos("in28Minutes"); 
		Collections.sort(listTodo, new NameComparator());
		
		for(Todo todo : listTodo)
		{
			System.out.println(todo);
		}
	
	}

}
